# Environment
![](assets/models/3D/environments/preview.png)
*Everything is in one blender-file (separated by collections), but the FBX models are separated.*

# Weapons
![](assets/models/3D/weapons/spas-12/preview.png)
*Franchi SPAS-12*

![](assets/models/3D/weapons/crowbar/preview.png)
*Crowbar*

# Ammunition
![](assets/models/3D/weapons/ammunition/preview.png)
*Currently included: 12 gauge shotshells*

# [Saucy](https://saucy.se)'s GameDev collections
I'd like to think that you can't copyright ideas. Everything in here is free to use in whatever context you'd like. I am the creator of these assets (except for the voxel palette - credited below).

I only hope to inspire others as they have inspired me!


## /assets/audio/sfx
- Created using [sfxr](http://www.drpetter.se/project_sfxr.html)


## /assets/models/3D
- Created using [Blender](https://www.blender.org/)
- Screenshot previews of all models are [available here](preview-3D-models.md)


## /assets/images
- Created using [Aseprite](https://www.aseprite.org/)
- Created using [Photoshop](https://www.adobe.com/products/photoshop.html)


## /assets/models/voxel
- Created using [MagicaVoxel](https://ephtracy.github.io) by [@ephtracy](https://github.com/ephtracy)
- Palette based on reddit post by [@Valaadus](https://www.reddit.com/r/TroveCreations/comments/2iqj6t/more_userfriendly_palette_for_magicavoxel/) (I moved the black/white colors together)


## License
[MIT License](LICENSE.md)
